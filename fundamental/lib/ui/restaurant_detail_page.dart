import 'package:restaurant/data/model/restaurant.dart';
import 'package:flutter/material.dart';

class RestaurantDetailPage extends StatelessWidget {
  static const routeName = '/restaurant_detail';

  final Restaurant restaurant;

  const RestaurantDetailPage({Key? key, required this.restaurant}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Restaurant App'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Hero(
                tag: restaurant.name,
                child: Image.network(restaurant.pictureId)),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    restaurant.name,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      const Icon(Icons.location_on_outlined),
                      Text(
                        restaurant.city,
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      const Icon(Icons.star_border, color: Colors.yellow,),
                      Text(
                        restaurant.rating.toString(),
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),
                  const Divider(color: Colors.grey,),
                  Text(
                    restaurant.description,
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                  const Divider(color: Colors.grey,),
                  Text(
                    'Food',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: restaurant.menus.foods.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Center(
                              child: Card(
                                color: Colors.blue,
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(restaurant.menus.foods[index].name.toString(), style: const TextStyle(color: Colors.white),)
                                ),
                              ),
                          );
                        },
                    ),
                  ),
                  const SizedBox(height: 10,),
                  Text(
                    'Beverages',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                  Container(
                    height: 50,
                    width: double.infinity,
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: restaurant.menus.drinks.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Center(
                              child: Card(
                                color: Colors.blue,
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text(restaurant.menus.drinks[index].name.toString(), style: const TextStyle(color: Colors.white),)
                                ),
                              ),
                          );
                        },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}