import 'package:restaurant/common/styles.dart';
import 'package:restaurant/data/model/restaurant.dart';
import 'package:restaurant/ui/restaurant_detail_page.dart';
import 'package:restaurant/widgets/platform_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RestaurantListPage extends StatelessWidget {
  const RestaurantListPage({Key? key}) : super(key: key);

  Widget _buildList(BuildContext context) {
    return FutureBuilder<String>(
      future: DefaultAssetBundle.of(context).loadString('assets/local_restaurant.json'),
      builder: (context, snapshot) {
        final List<Restaurant> restaurants = parseRestaurants(snapshot.data);
        return ListView.builder(
          itemCount: restaurants.length,
          itemBuilder: (context, index) {
            return _buildRestaurantItem(context, restaurants[index]);
          },
        );
      },
    );
  }

  Widget _buildRestaurantItem(BuildContext context, Restaurant restaurant) {
    int fullYellowStars = restaurant.rating.floor();
    int halfYellowStar = (restaurant.rating - fullYellowStars) >= 0.1 && (restaurant.rating - fullYellowStars) <= 0.9 ? 1 : 0;
    int whiteStar = 5 - restaurant.rating.floor() - halfYellowStar;
    return Material(
      color: primaryColor,
      child: ListTile(
        contentPadding:
          const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        leading: Hero(
          tag: restaurant.name,
          child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                restaurant.pictureId,
                width: 100,
                fit: BoxFit.cover,
              ),
          ),
        ),
        title: Text(
          restaurant.name,
        ),
        subtitle: Column(
          children: [
            Row(
              children: [
                const Icon(Icons.location_on_outlined),
                Text(restaurant.city)
              ],
            ),
            Row(
              children: [
                Row(
                  children: List.generate(
                    fullYellowStars,
                    (_) => const Icon(Icons.star, color: Colors.yellow),
                ) +
                List.generate(
                    halfYellowStar,
                    (_) => const Icon(Icons.star_half, color: Colors.yellow),
                ) +
                List.generate(
                    whiteStar,
                    (_) => const Icon(Icons.star_outline, color: Colors.yellow),
                ),
                ),
                Text(restaurant.rating.toString())
              ],
            ),
          ],
        ),
        onTap: () {
          Navigator.pushNamed(context, RestaurantDetailPage.routeName,
              arguments: restaurant.id);
        },
      ),
    );
  }

  Widget _buildAndroid(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Restaurant App'),
      ),
      body: _buildList(context),
    );
  }

  Widget _buildIos(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Restaurant App'),
        transitionBetweenRoutes: false,
      ),
      child: _buildList(context),
    );
  }

  @override
  Widget build(BuildContext context) {
    return PlatformWidget(
      androidBuilder: _buildAndroid,
      iosBuilder: _buildIos,
    );
  }
}