import 'package:restaurant/widgets/custom_scaffold.dart';
import 'package:flutter/widgets.dart';
import 'package:webview_flutter/webview_flutter.dart';

class RestaurantWebView extends StatelessWidget {
  static const routeName = '/restaurant_web';

  final String url;

  RestaurantWebView({Key? key, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  final controller = WebViewController()
  ..setJavaScriptMode(JavaScriptMode.unrestricted)
  ..setBackgroundColor(const Color(0x00000000))
  ..setNavigationDelegate(
    NavigationDelegate(
      onProgress: (int progress) {
        // Update loading bar.
      },
      onPageStarted: (String url) {},
      onPageFinished: (String url) {},
      onWebResourceError: (WebResourceError error) {},
      onNavigationRequest: (NavigationRequest request) {
        if (request.url.startsWith('https://www.youtube.com/')) {
          return NavigationDecision.prevent;
        }
        return NavigationDecision.navigate;
      },
    ),
  )
  ..loadRequest(Uri.parse(url));
    return CustomScaffold(
      body: WebViewWidget(controller: controller),
    );
  }
}