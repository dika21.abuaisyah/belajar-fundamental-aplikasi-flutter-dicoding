class Food {
 final String name;

 Food({required this.name});

 factory Food.fromJson(Map<String, dynamic> food) {
    return Food(
      name: food['name'],
    );
 }
}