import 'dart:convert';
import 'package:restaurant/data/model/menu.dart';

class Restaurant {
 final String id;
 final String name;
 final String description;
 final String pictureId;
 final String city;
 final double rating;
 final Menus menus;

 Restaurant({
    required this.id,
    required this.name,
    required this.description,
    required this.pictureId,
    required this.city,
    required this.rating,
    required this.menus,
 });

 factory Restaurant.fromJson(Map<String, dynamic> restaurant) {
    return Restaurant(
      id: restaurant['id'],
      name: restaurant['name'],
      description: restaurant['description'],
      pictureId: restaurant['pictureId'],
      city: restaurant['city'],
      rating: restaurant['rating'].toDouble(),
      menus: Menus.fromJson(restaurant['menus']),
    );
 }
}

List<Restaurant> parseRestaurants(String? json) {
 if (json == null) {
    return [];
 }

 final Map<String, dynamic> parsed = jsonDecode(json);
 final List<dynamic> restaurantsJson = parsed['restaurants'];
 return restaurantsJson.map((restaurantJson) => Restaurant.fromJson(restaurantJson)).toList();
}

