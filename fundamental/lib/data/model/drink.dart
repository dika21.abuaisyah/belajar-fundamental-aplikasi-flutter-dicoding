class Drink {
 final String name;

 Drink({required this.name});

 factory Drink.fromJson(Map<String, dynamic> drink) {
    return Drink(
      name: drink['name'],
    );
 }
}