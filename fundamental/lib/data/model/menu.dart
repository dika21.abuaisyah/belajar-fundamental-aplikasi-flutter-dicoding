import 'package:restaurant/data/model/food.dart';
import 'package:restaurant/data/model/drink.dart';

class Menus {
 final List<Food> foods;
 final List<Drink> drinks;

 Menus({required this.foods, required this.drinks});

 factory Menus.fromJson(Map<String, dynamic> menus) {
    var foodsFromJson = (menus['foods'] as List)
        .map((i) => Food.fromJson(i))
        .toList();
    var drinksFromJson = (menus['drinks'] as List)
        .map((i) => Drink.fromJson(i))
        .toList();

    return Menus(
      foods: foodsFromJson,
      drinks: drinksFromJson,
    );
 }
}